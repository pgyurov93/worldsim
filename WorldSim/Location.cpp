#include "Location.h"
#include <iostream>
#include <stdio.h>
#include <Windows.h>
#include <numeric>
#include <algorithm>

void Location::set_BD_rates(double birth_rate, double death_rate)
{
	_birth_rate = birth_rate;
	_death_rate = death_rate;
}

void Location::set_hurricane_stats(double probability, double casualty_multiplier)
{
	_hurricane.set_parameters(probability, casualty_multiplier);
}


void Location::simulate_population()
{
	_current_population = 0;

	_hurricane.simulate_natural_disaster();
	_total_casualty_factor = std::accumulate(_all_casualty_multipliers.begin(), _all_casualty_multipliers.end(), 0.0);

	for (double t = 0; t <= _maximum_time; t = t + _time_scaling)
	{
		double numerator = (_maximum_population * _initial_population * exp(_growth_rate * t));
		double denomintator = (_maximum_population + _initial_population * (exp(_growth_rate * t) - 1));
		_current_population = numerator / denomintator - _total_casualty_factor * _current_population;;
		std::cout << _current_population << "\t" << t << std::endl;
		Sleep(100);
		//system("cls");
	}
}