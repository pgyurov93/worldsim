#ifndef HURRICANE_H
#define HURRICANE_H
#include "Environment.h"

class Hurricane : public Environment
{
public:
	void set_parameters(double, double);
	void simulate_natural_disaster();
};

#endif