#ifndef RANDOM_H
#define RANDOM_H

class Random
{
private:
	double _random_number;

public:
	double generate_Gaussian_random_number(double, double);
};


#endif