#include "Hurricane.h"
#include <stdlib.h> 
#include <time.h>   

void Hurricane::set_parameters(double probability, double casualty_multiplier)
{
	_probability = probability;
	_casualty_multipler = casualty_multiplier;
}

void Hurricane::simulate_natural_disaster()
{
	_decider_value = static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
	if (_decider_value == _probability)
		_all_casualty_multipliers.push_back(_casualty_multipler);

}

void Environment::initialise_random_seed()
{
	srand(time(NULL));
}