#ifndef LOCATION_H
#define LOCATION_H
//#include "World.h"
#include "Population.h"
#include "Hurricane.h"

class Location: public Population, public Environment
{
private:
	double _birth_rate, _death_rate;
	double _total_casualty_factor;
	Hurricane _hurricane;

public:
	void set_BD_rates(double, double);
	
	void set_hurricane_stats(double, double);
	//rest of nat dis stats funcs here
	
	void simulate_population();


};

#endif