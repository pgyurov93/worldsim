#include "Population.h"
#include "Environment.h"
#include "Location.h"

int main()
{
	Environment env;
	Population pop;
	Location X(1,1);
	env.initialise_random_seed();

	pop.set_initial_population(1000);
	pop.set_growth_rate(0.001);
	pop.set_maximum_population(5000);
	pop.set_maximum_time(1000);
	pop.set_timescaling(1);

	//X.simulate_population();
}